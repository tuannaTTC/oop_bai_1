package main;

import IBussines.ICusForBussines;
import IBussines.ICusVnBussines;
import bussines.CusForBussines;
import bussines.CusVnBussines;

import java.text.ParseException;
import java.util.Scanner;

public class Main {
   
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int luachon = 0;
        String _continue = "y";

        ICusVnBussines iCusVnBussines = new CusVnBussines();
        ICusForBussines iCusForBussines = new CusForBussines();

        while (_continue.equals("y")){
            showMenu();
            System.out.print("Nhập lựa chọn: ");
            luachon = sc.nextInt();
            switch (luachon){
                case 1:
                    int khachHang = 0;
                    System.out.print("1. Khách hàng nước ngoài \n");
                    System.out.print("2. Khách hàng Việt Nam \n");
                    khachHang = sc.nextInt();
                    switch (khachHang){
                        case 1:
                            iCusForBussines.inputCustomerForeign();
                            break;
                        case 2:
                            iCusVnBussines.inputCustomerVn();
                            break;
                    }
                    _continue = isContinue();
                    break;
                case 2:
                    iCusForBussines.addDataForeign();
                    iCusVnBussines.addDataVN();
                    _continue = isContinue();
                    break;
                case 3:
                    iCusForBussines.outPut();
                    iCusVnBussines.outPut();
                    _continue = isContinue();
                    break;
                case 4:
                    iCusVnBussines.countType();
                    _continue = isContinue();
                    break;
                case 5:
                    System.out.print("Tổng tiền của khách hàng nước ngoài: " + iCusForBussines.mean() + "\n");
                    _continue = isContinue();
                    break;
                case 6:
                    iCusForBussines.checkDate(7,2020);
                    iCusVnBussines.checkDate(7,2020);
                    _continue = isContinue();
                    break;
            }
        }
    }

    public static void showMenu(){
        System.out.print("\n");
        System.out.print("1. Nhập dữ liệu console. \n");
        System.out.print("2. Nhập dữ liệu bằng có sẵn. \n");
        System.out.print("3. Xuất dữ liệu tất cả khách hàng. \n");
        System.out.print("4. Tổng số lượng cho từng loại khách hàng. \n");
        System.out.print("5. Tính trung bình thành tiền của khách hàng nước ngoài. \n");
        System.out.print("6. Xuất ra hóa đơn trong tháng 07 năm 2020 \n");
        System.out.print("<========================================================> \n");
    }

    public static String isContinue(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Bạn có muốn tiếp tục sử dụng (y/n)?: ");
        String _continue = sc.next();
        return _continue;
    }

}