package bussines;

import IBussines.ICusBessines;
import IBussines.ICusForBussines;
import model.Constant;
import model.Customer;
import model.CustomerForeign;
import model.CustomerVN;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class CusForBussines extends CusBussines implements ICusForBussines {
    private ArrayList<CustomerForeign> customerForeigns;

    public CusForBussines() {
        this.customerForeigns = new ArrayList<>();
    }

    @Override
    public void inputCustomerForeign(){
        Scanner sc = new Scanner(System.in);

        CustomerForeign customerForeign = new CustomerForeign();
        Customer customer = inputCustomer();

        customerForeign.setCustemorId(customer.getCustemorId());
        customerForeign.setName(customer.getName());
        customerForeign.setDate(customer.getDate());
        customerForeign.setQuantity(customer.getQuantity());
        customerForeign.setUnitPrice(customer.getUnitPrice());
        customerForeign.setLimit(customer.getLimit());

        System.out.print("Quốc tịch: ");
        customerForeign.setNationality(sc.next());

        customerForeign.totalAmount();
        this.customerForeigns.add(customerForeign);
    }

    public Double mean(){
        Double sum = 0.0;
        for (int i = 0 ; i < customerForeigns.size() ; i ++){
            sum += customerForeigns.get(i).getAmount();
        }
        return sum/(customerForeigns.size());
    }

    public void outPut(){
        System.out.print("Danh sách khách ngoại quốc: \n");
        customerForeigns.forEach(System.out::println);
    }

    @Override
    public void addDataForeign(){
        try {
            CustomerForeign customerForeign = new CustomerForeign("07","Messi_01",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),110.0, Constant.UNIT_PRICE, Constant.LIMIT,"Hàn");
            CustomerForeign customerForeign1 = new CustomerForeign("08","Messi_02",new SimpleDateFormat("dd/MM/yyyy").parse("21/08/2020"),130.0,Constant.UNIT_PRICE, Constant.LIMIT,"Hàn");
            CustomerForeign customerForeign2 = new CustomerForeign("09","Messi_03",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),170.0,Constant.UNIT_PRICE, Constant.LIMIT,"Nhật");
            CustomerForeign customerForeign3 = new CustomerForeign("10","Messi_04",new SimpleDateFormat("dd/MM/yyyy").parse("21/09/2020"),200.0,Constant.UNIT_PRICE, Constant.LIMIT,"Nhật");
            CustomerForeign customerForeign4 = new CustomerForeign("11","Messi_05",new SimpleDateFormat("dd/MM/yyyy").parse("20/07/2020"),245.0,Constant.UNIT_PRICE, Constant.LIMIT,"Nhật");
            CustomerForeign customerForeign5 = new CustomerForeign("12","Messi_06",new SimpleDateFormat("dd/MM/yyyy").parse("18/07/2020"),320.0,Constant.UNIT_PRICE, Constant.LIMIT,"Hàn");

            customerForeigns.add(customerForeign);
            customerForeigns.add(customerForeign1);
            customerForeigns.add(customerForeign2);
            customerForeigns.add(customerForeign3);
            customerForeigns.add(customerForeign4);
            customerForeigns.add(customerForeign5);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkDate(Integer month, Integer year) {
        for(int i = 0 ; i < customerForeigns.size() ; i ++){
                        if(isDate(customerForeigns.get(i).getDate(),7,2020)){
                            System.out.println(customerForeigns.get(i).toString());
                        }
                    }
    }
}
