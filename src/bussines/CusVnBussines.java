package bussines;

import IBussines.ICusVnBussines;
import model.Constant;
import model.Customer;
import model.CustomerVN;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class CusVnBussines extends CusBussines implements ICusVnBussines {
    private ArrayList<CustomerVN> customerVNS;

    public CusVnBussines(){
        this.customerVNS = new ArrayList<>();
    }

    @Override
    public void inputCustomerVn(){
        Scanner sc = new Scanner(System.in);

        CustomerVN customerVN= new CustomerVN();
        Customer customer = super.inputCustomer();

        customerVN.setCustemorId(customer.getCustemorId());
        customerVN.setName(customer.getName());
        customerVN.setDate(customer.getDate());
        customerVN.setQuantity(customer.getQuantity());
        customerVN.setUnitPrice(customer.getUnitPrice());
        customerVN.setLimit(customer.getLimit());

        System.out.print("Đối tượng khách hàng: ");
        customerVN.setCustomerType(sc.next());

        customerVN.totalAmount();
        this.customerVNS.add(customerVN);
    }

    public void countType(){
        int countSinhHoat = 0;
        int countSanXuat = 0;
        int countKinhDoanh = 0;
        for (int i = 0 ; i < customerVNS.size(); i++){

            switch (customerVNS.get(i).getCustomerType()) {
                case "Sinh hoạt":
                    countSinhHoat += 1;
                    break;
                case "Sản xuất":
                    countSanXuat += 1;
                    break;
                case "Kinh doanh":
                    countKinhDoanh += 1;
                    break;
            }
        }
        System.out.print("Tổng số khách hàng là hộ sinh hoạt: " + countSinhHoat + "\n");
        System.out.print("Tổng số khách hàng là hộ Sản xuất: " + countSanXuat + "\n");
        System.out.print("Tổng số khách hàng là hộ kinh doanh: " + countKinhDoanh + "\n");
    }
    public void outPut(){
        System.out.print("Danh sách khách Việt nam: \n");
        customerVNS.forEach(System.out::println);
    }

    public void addDataVN(){
        try {
            CustomerVN customerVN = new CustomerVN("01","Anh Tuan_01",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),100.0, Constant.UNIT_PRICE, Constant.LIMIT,"Sinh hoạt");
            CustomerVN customerVN1 = new CustomerVN("02","Anh Tuan_02",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),150.0,Constant.UNIT_PRICE, Constant.LIMIT,"Kinh doanh");
            CustomerVN customerVN2 = new CustomerVN("03","Anh Tuan_03",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),170.0,Constant.UNIT_PRICE, Constant.LIMIT,"Kinh doanh");
            CustomerVN customerVN3 = new CustomerVN("04","Anh Tuan_04",new SimpleDateFormat("dd/MM/yyyy").parse("21/07/2020"),220.0,Constant.UNIT_PRICE, Constant.LIMIT,"Sinh hoạt");
            CustomerVN customerVN4 = new CustomerVN("05","Anh Tuan_05",new SimpleDateFormat("dd/MM/yyyy").parse("21/09/2020"),250.0,Constant.UNIT_PRICE, Constant.LIMIT,"Sản xuất");
            CustomerVN customerVN5 = new CustomerVN("06","Anh Tuan_06",new SimpleDateFormat("dd/MM/yyyy").parse("21/09/2020"),300.0,Constant.UNIT_PRICE, Constant.LIMIT,"Sinh hoạt");

            customerVNS.add(customerVN);
            customerVNS.add(customerVN1);
            customerVNS.add(customerVN2);
            customerVNS.add(customerVN3);
            customerVNS.add(customerVN4);
            customerVNS.add(customerVN5);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void checkDate(Integer month,Integer year) {
        for(int i = 0 ; i < customerVNS.size() ; i ++){
                        if(isDate(customerVNS.get(i).getDate(),month,year)){
                            System.out.println(customerVNS.get(i).toString());
                        }
                    }
    }


}
