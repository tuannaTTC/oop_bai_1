package bussines;

import IBussines.ICusBessines;
import model.Customer;
import model.CustomerForeign;
import model.CustomerVN;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class CusBussines implements ICusBessines {
    private ArrayList<Customer> customers;

    @Override
    public Customer inputCustomer(){
        Scanner sc = new Scanner(System.in);
        Customer customer = new Customer();

        System.out.print("Mã khách hàng: ");
        customer.setCustemorId(sc.next());
        System.out.print("Họ và tên: ");
        customer.setName(sc.next());
        System.out.print("Ngày ra hóa đơn: ");
        try {
            customer.setDate(new SimpleDateFormat("dd/MM/yyyy").parse(sc.next()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.print("Số lượng: ");
        customer.setQuantity(sc.nextDouble());
        System.out.print("Đơn giá: ");
        customer.setUnitPrice(sc.nextDouble());
        System.out.print("Định mức: ");
        customer.setLimit(sc.nextDouble());

        return customer;
    }

    @Override
    public boolean isDate(Date date, Integer month, Integer year){
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Integer thisMonth = localDate.getMonthValue();
        Integer thisYear = localDate.getYear();
        if (month.equals(thisMonth)&& year.equals(thisYear)){
            return true;
        }
        else {
            return false;
        }
    }

}
