package IBussines;

import model.Customer;

import java.util.Date;

public interface ICusBessines {
    Customer inputCustomer();
    boolean isDate(Date date, Integer month, Integer year);
}
