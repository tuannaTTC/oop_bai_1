package model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

public class Customer {
    private String custemorId;
    private String name;
    private Date date;
    private Double quantity;
    private Double unitPrice;
    private Double limit;
    private Double amount;



    public Customer(String custemorId, String name, Date date, Double quantity, Double unitPrice, Double limit) {
        this.custemorId = custemorId;
        this.name = name;
        this.date = date;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.limit = limit;
    }

    public Customer() {

    }

    public String getCustemorId() {
        return custemorId;
    }

    public void setCustemorId(String custemorId) {
        this.custemorId = custemorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
    @Override
    public String toString() {
        return " custemorId='" + custemorId + '\'' +
                ", name='" + name + '\'' +
                ", date='" + new SimpleDateFormat("dd/MM/yyyy").format(date) + '\'' +
                ", quantity=" + quantity +
                ", unitPrice=" + unitPrice +
                ", limit=" + limit + ", amount =" +amount ;
    }
}
