package model;

import java.util.Date;

public class CustomerForeign extends Customer {
    private String nationality;

    public CustomerForeign(String custemorId, String name, Date date, Double quantity, Double unitPrice, Double limit, String nationality) {
        super(custemorId, name, date, quantity, unitPrice, limit);
        this.nationality = nationality;
        this.totalAmount();
    }

    public CustomerForeign() {

    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void totalAmount(){
        Double amuont = 0.0;
        amuont = super.getQuantity()*super.getUnitPrice();
        super.setAmount(amuont);
    }

    @Override
    public String toString() {
        super.toString();
        return "CustomerForeign{" + super.toString() +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
