package model;

import java.util.Date;

public class CustomerVN extends Customer {
    private String customerType;

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public CustomerVN(String custemorId, String name, Date date, Double quantity, Double unitPrice, Double limit, String customerType) {
        super(custemorId, name, date, quantity, unitPrice, limit);
        this.customerType = customerType;
        this.totalAmount();


    }


    public void totalAmount(){
        Double amuont = 0.0;
        if(super.getQuantity() < super.getLimit()){
            amuont = super.getQuantity() * super.getUnitPrice();
        }
        else {
            amuont = super.getQuantity() * super.getQuantity() * super.getLimit() + (super.getQuantity() - super.getLimit())*super.getUnitPrice() * 2.5;
        }
        super.setAmount(amuont);
    }


    public String getCustomerType() {
        return customerType;
    }

    public CustomerVN() {

    }

    @Override
    public String toString() {

        return "CustomerVN{" + super.toString() +
                ", customerType='" + customerType + '\'' +
                '}';
    }
}
